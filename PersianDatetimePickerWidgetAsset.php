<?php

namespace winkers\persianDatetimePickerWidget;

use yii\web\AssetBundle;

/**
 * Class PersianDatetimePickerWidgetAsset
 * @package winkers\persianDatetimePickerWidget
 */
class PersianDatetimePickerWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $css = [
        'css/jquery.Bootstrap-PersianDateTimePicker.css'
    ];

    public $js = [
        'js/jalaali.js',
        'js/jquery.Bootstrap-PersianDateTimePicker.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}