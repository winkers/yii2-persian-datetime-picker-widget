
Persian datetime picker widget for Yii2  
=====================================  
Persian datetime picker widget for Yii2  
  
Read this in other languages: [English](README.md) 

Table of Contents
------------ 
  - [Installation](#installation)
    - [Use extension](#use-extension)
    - [Call widget](#call-widget)
  - [Usage](#usage)
  - [Screenshots](#screenshots)
  - [Customization](#customization)
  - [Examples](#examples)
  - [License](#licence)
  - [Contributions](#contributions)

Installation 
------------  
  
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).  
  
Either run  
  
```  
php composer.phar require --prefer-dist winkers/yii2-persian-datetime-picker-widget "*"  
```  
  
or add  
  
```  
"winkers/yii2-persian-datetime-picker-widget": "*"  
```  
  
to the require section of your `composer.json` file.  
  
  
Usage
-----  

Once the extension is installed, simply follow bellow steps  :  
  
### Use extension

Simply use the extension in your view file :  

```php  
use winkers\persianDatetimePickerWidget\PersianDatetimePickerWidget;  
```  
  
### Call widget

Basic usage :  

```php
<?= PersianDatetimePickerWidget::widget(); ?>  
```
   
Usage with `model` :  

```php
<?= PersianDatetimePickerWidget::widget([  
    'model' => $model,  
    'attribute' => 'attribute'  
]) ?>  
```  

Usage with `ActiveForm` :  

```php
<?= $form->field($model, 'attribute')->widget(PersianDatetimePickerWidget::className()) ?>  
```  

Screenshots
-----
This is a screenshot of the extension :

![enter image description here](https://lh3.googleusercontent.com/S16EfZnWUrIZWqbe4lhMhZWjVXsCTYLYbVMS8xfWYiyyxIYcNngeTXucnFLYZjV6JdkeJ6_iNE2r "Persian datetime picker widget")

Customization
-----    

Use this options to customize widget for your requirements :

`icon`  
- value type : `bool`
- default value : `true`

`iconOptions`
- value type : `array`
- default value : `['class' => 'glyphicon glyphicon-calendar']`

`addonOptions`
- value type : `array`
- default value : `['class' => 'input-group-addon']`

`options`
- value type : `array`
- default value : `['class' => 'form-control']`

`settings`

- `placement`
	- value type : `string`
	- value : `left` | `top` | `right` | `bottom` 
	- default type : `bottom`

- `trigger`
	- value type : `string`
	- value : `click` | `focus` | `mouseover` 
	- default type : `click` 

- `format`
	- value type : `string`
	- value : 
		- `yyyy` : 4 digit year
		- `yy` : 2 digit year
		- `MMMM` : persian month name
		- `MM` : 2 digit month
		- `M` : 1 digit month
		- `dddd` : persian day name
		- `dd` : 2 digit day
		- `d` : 1 digit day
		- `HH` : 2 digit hour from 00 to 24
		- `H` : 1 digit hour from 0 to 24
		- `hh` : 2 digit hour from 00 to 12
		- `h` : 1 digit hour from 0 to 12
		- `mm` : 2 digit minutes
		- `m` : 1 digit minutes
		- ...
	- default type : `yyyy-MM-dd` 

- `groupId`
	- value type : `string`
	- default value : No Default Value

- `fromDate`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`
	
- `toDate`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`

- `disableBeforeToday`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`

- `disabled`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`

- `enableTimePicker`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`

- `isGregorian`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`
	
- `englishNumber`
	- value type : `string`
	- value : `true` | `false`
	- default value : `false`
	
Examples
-----  

This is an example of widget usage : 

```php
<?= $form->field($model, 'attribute')->widget(PersianDatetimePickerWidget::className(), [
    'icon' => true,
    'iconOptions' => [
        'class' => 'glyphicon glyphicon-calendar'
    ],
    'settings' => [
        'enableTimePicker' => 'true',
        'format' => 'yyyy-MM-dd HH:mm',
        'placement' => 'left',
    ]
])
?>
```

License
-----  

This project  is released under `GNU General Public License v2.0`. Look at [LICENSE](LICENSE) for details.

Contributions
-----
I wrote this extension for Yii2.
Thanks to [Mohammmad Dayyan](https://github.com/Mds92) for [MD.BootstrapPersianDateTimePicker](https://github.com/Mds92/MD.BootstrapPersianDateTimePicker) and releasing this wonderful `javascript` datepicker under `The MIT License`.