<?php

namespace winkers\persianDatetimePickerWidget;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class PersianDatetimePickerWidget
 * @package winkers\persianDatetimePickerWidget
 */
class PersianDatetimePickerWidget extends Widget
{
    public $id;
    public $name;
    public $model;
    public $attribute;
    public $form;
    public $formLabel = true;

    public $icon = true;
    public $addonOptions;
    public $iconOptions;
    public $options = [];
    public $settings;

    private $settingMap = [
        'placement' => 'placement',
        'trigger' => 'trigger',
        'format' => 'mdformat',
        'groupId' => 'groupid',
        'fromDate' => 'fromdate',
        'toDate' => 'todate',
        'disableBeforeToday' => 'disablebeforetoday',
        'disabled' => 'disabled',
        'enableTimePicker' => 'enabletimepicker',
        'isGregorian' => 'isgregorian',
        'englishNumber' => 'englishnumber',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->configWidget();
        $this->registerAssets();
    }

    /**
     * Register assets
     */
    public function registerAssets()
    {
        PersianDatetimePickerWidgetAsset::register($this->view);
    }

    /**
     * Config widget
     */
    public function configWidget()
    {
        $this->configField();
        $this->configOptions();
        $this->configSettings();
    }

    /**
     * Config Field
     */
    public function configField()
    {
        if (!$this->id) {
            if ($this->model and $this->attribute) {
                $this->id = Html::getInputId($this->model, $this->attribute);
            } else {
                $this->id = "datetimepicker";
            }
        }
        if (!$this->model)
            $this->model = null;
        if (!$this->name) {
            $this->name = "datetimepicker";
        }
        if ($this->attribute)
            $this->name = $this->attribute;
        if (!$this->form)
            $this->form = null;
    }

    /**
     * Config options
     */
    public function configOptions()
    {
        $this->iconOptions = $this->configDefaultOptions($this->iconOptions, ['class' => 'glyphicon glyphicon-calendar']);
        $this->addonOptions = $this->configDefaultOptions($this->addonOptions, ['class' => 'input-group-addon']);
        $this->options = $this->configDefaultOptions($this->options, ['class' => 'form-control']);
    }

    /**
     * config settings
     */
    public function configSettings()
    {
        $this->addonOptions['data-mddatetimepicker'] = 'true';
        $this->addonOptions['data-targetselector'] = '#' . $this->id;
        foreach ($this->settings as $key => $value) {
            $dataName = $this->settingMap[$key];
            $this->addonOptions['data-' . $dataName] = $value;
        }
    }

    /**
     * Config default options
     * @param $array
     * @param $default
     * @return array
     */
    public function configDefaultOptions($array, $default)
    {
        if (is_array($array)) {
            foreach ($default as $key => $value) {
                if (!array_key_exists($key, $array)) {
                    $array[$key] = $value;
                }
            }
            return $array;
        } else {
            return $default;
        }
    }

    /**
     * Render Widget
     */
    public function renderWidget()
    {
        if ($this->icon) {
            $addonContent = Html::tag('i', '', $this->iconOptions);
            echo Html::tag('div', $addonContent, $this->addonOptions);
        } else {
            echo Html::tag('div', '', $this->addonOptions);
        }

        if ($this->form) {
            if (!$this->formLabel)
                echo $this->form->field($this->model, $this->attribute)->textInput($this->options)->label(false);
            else
                echo $this->form->field($this->model, $this->attribute)->textInput($this->options);
        } else if ($this->model) {
            echo Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textInput($this->name, $this->options);
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->renderWidget();
    }
}
